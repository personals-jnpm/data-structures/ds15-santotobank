package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Cliente {

    private int idCliente;
    private String nombre;
    private String cedula;
    private List<Cuenta> cuentas;
    private Stack<Integer> acciones;

    public Cliente() {
        cuentas = new ArrayList<>();
        acciones = new Stack<>();
    }

    public Cliente(int idCliente, String nombre, String cedula, List<Cuenta> cuentas, Stack<Integer> acciones) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.cedula = cedula;
        this.cuentas = cuentas;
        this.acciones = acciones;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public List<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public Stack<Integer> getAcciones() {
        return acciones;
    }

    public void setAcciones(Stack<Integer> acciones) {
        this.acciones = acciones;
    }

    @Override
    public String toString() {
        return "Cliente {\n" +
                " \t id: " + idCliente +
                ", nombre: " + nombre +
                ", cedula: " + cedula +
                ",\n \t cuentas " + cuentas +
                ", \n \t acciones: " + acciones +
                "}\n";
    }

    public void agregarCuenta(Cuenta cuenta) {
        cuentas.add(cuenta);
    }

    public void agregarAccion(int element) {
        acciones.add(element);
    }

    public Cuenta obtenerCuentaPorNumero(int numeroCuenta) {
        for (Cuenta cuenta : cuentas) {
            if (cuenta.getNumeroCuenta() == numeroCuenta) {
                return cuenta;
            }
        }
        return null;
    }

    public void validarInfoCuenta(int numeroCuenta, String clave) throws Exception {
        try {
            Cuenta cuenta = obtenerCuentaPorNumero(numeroCuenta);
            if (!cuenta.getClave().equals(clave)) {
                throw new Exception("  ¡Clave incorrecta!");
            }
        } catch (NullPointerException e) {
            throw new Exception("  ¡Número de cuenta incorrecto!");
        }
    }

    public void imprimirCuentas() {
        System.out.println("\n \tCuentas de " + nombre + " [");
        for (Cuenta cuenta : cuentas) {
            System.out.println("\t \t" + cuenta.toString());
        }
        System.out.println("\t]");
    }
}
