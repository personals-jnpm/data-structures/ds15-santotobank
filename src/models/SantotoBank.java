package models;

import java.util.*;

public class SantotoBank {

    private List<Cliente> clientes;
    private Queue<Cliente> colaClientes;

    public SantotoBank() {
        clientes = new ArrayList<>();
        colaClientes = new LinkedList<>();

        initClientes();
        generateRandomOrder();
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Queue<Cliente> getColaClientes() {
        return colaClientes;
    }

    public void setColaClientes(Queue<Cliente> colaClientes) {
        this.colaClientes = colaClientes;
    }

    public void atenderCliente() {
        colaClientes.poll();
    }

    public Cliente obtenerClienteActual() {
        return colaClientes.peek();
    }

    public void imprimirColaClientes() {
        System.out.println("Clientes en cola [");
        for (Cliente cliente : colaClientes) {
            System.out.println("\t" + cliente.toString());
        }
        System.out.print("]\n");
    }

    public void imprimirClientes() {
        System.out.println("Clientes del banco [");
        for (Cliente cliente : clientes) {
            System.out.println("\t" + cliente.toString());
        }
        System.out.print("]\n");
    }

    private void generateRandomOrder() {
        List<Integer> listaTemporal = new ArrayList<>();
        for (int i = 0; i < clientes.size() - 1; i++) {
            int idCliente = (int) (Math.random() * (10));
            if (i == 0) {
                listaTemporal.add(idCliente);
                colaClientes.add(buscarCliente(idCliente));
            } else if (!listaTemporal.contains(idCliente)) {
                listaTemporal.add(idCliente);
                colaClientes.add(buscarCliente(idCliente));
            }
        }
    }

    public Cliente buscarCliente(int id) {
        for (Cliente cliente : clientes) {
            if (cliente.getIdCliente() == id) {
                return cliente;
            }
        }
        return null;
    }

    private void initClientes() {
        clientes.add(new Cliente(0, "Carlos", "1254123654",
                new ArrayList<>(Arrays.asList(new Cuenta(4528, "ki78", 50000), new Cuenta(4578, "qe53", 70000),
                        new Cuenta(3578, "br32", 59000))), new Stack<>()));
        clientes.add(new Cliente(1, "María", "5423687",
                new ArrayList<>(Arrays.asList(new Cuenta(4235, "er48", 83000), new Cuenta(2356, "nj30", 49000),
                        new Cuenta(3698, "er78", 100000))), new Stack<>()));
        clientes.add(new Cliente(2, "Diana", "45678214",
                new ArrayList<>(Arrays.asList(new Cuenta(1236, "uy41", 63000), new Cuenta(1000, "re20", 78000),
                        new Cuenta(7842, "bt12", 52500))), new Stack<>()));
        clientes.add(new Cliente(3, "Oscar", "1021458793",
                new ArrayList<>(Arrays.asList(new Cuenta(4362, "ki13", 40000), new Cuenta(7823, "qw78", 37000),
                        new Cuenta(2039, "cx78", 23000))), new Stack<>()));
        clientes.add(new Cliente(4, "Mariana", "4563217893",
                new ArrayList<>(Arrays.asList(new Cuenta(4125, "we45", 41000), new Cuenta(1368, "nj48", 89000),
                        new Cuenta(7426, "gb12", 75000))), new Stack<>()));
        clientes.add(new Cliente(5, "Catalina", "65478963",
                new ArrayList<>(Arrays.asList(new Cuenta(7265, "fr98", 15000), new Cuenta(1235, "yu28", 64000),
                        new Cuenta(7895, "wb78", 56300))), new Stack<>()));
        clientes.add(new Cliente(6, "Fernando", "1324563218",
                new ArrayList<>(Arrays.asList(new Cuenta(4265, "ff00", 81000), new Cuenta(3695, "er32", 87000),
                        new Cuenta(7532, "oi36", 45000))), new Stack<>()));
        clientes.add(new Cliente(7, "Andrés", "4521369",
                new ArrayList<>(Arrays.asList(new Cuenta(9634, "ñl16", 59000), new Cuenta(1201, "an53", 49000),
                        new Cuenta(4126, "rt00", 73000))), new Stack<>()));
        clientes.add(new Cliente(8, "Nicolás", "3256348521",
                new ArrayList<>(Arrays.asList(new Cuenta(7436, "wn46", 85000), new Cuenta(7126, "yu10", 150000))), new Stack<>()));
        clientes.add(new Cliente(9, "Laura", "7894521362",
                new ArrayList<>(Arrays.asList(new Cuenta(1039, "qc89", 96000), new Cuenta(6530, "ja30", 300000))), new Stack<>()));
    }
}
