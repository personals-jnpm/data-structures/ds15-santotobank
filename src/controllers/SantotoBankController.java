package controllers;

import models.Cliente;
import models.Cuenta;
import models.SantotoBank;

public class SantotoBankController {

    private final SantotoBank santotoBank;
    private Cliente clienteActual;
    private Cuenta cuentaActual;

    public SantotoBankController() {
        clienteActual = new Cliente();
        santotoBank = new SantotoBank();
    }

    public Cliente getClienteActual() {
        return clienteActual;
    }

    public void setClienteActual(Cliente clienteActual) {
        this.clienteActual = clienteActual;
    }

    public Cuenta getCuentaActual() {
        return cuentaActual;
    }

    public void setCuentaActual(Cuenta cuentaActual) {
        this.cuentaActual = cuentaActual;
    }

    public void agregarClienteActual() {
        clienteActual = santotoBank.obtenerClienteActual();
    }

    public void atenderCliente() {
        actualizarCliente();
        santotoBank.atenderCliente();
    }

    private void actualizarCliente() {
        clienteActual.getCuentas().set(clienteActual.getCuentas().indexOf(clienteActual.obtenerCuentaPorNumero
                (cuentaActual.getNumeroCuenta())), cuentaActual);
        santotoBank.getClientes().set(santotoBank.getClientes().indexOf(santotoBank.buscarCliente
                (clienteActual.getIdCliente())), clienteActual);
    }

    public void agregarAcciones(int element) {
        clienteActual.agregarAccion(element);
    }

    public boolean validarInfoCuenta(int numeroCuenta, String clave) {
        try {
            clienteActual.validarInfoCuenta(numeroCuenta, clave);
            cuentaActual = clienteActual.obtenerCuentaPorNumero(numeroCuenta);
            System.out.println("\n  ¡Credenciales correctas!\n");
            return true;
        } catch (Exception e) {
            System.out.println("\n" + e.getMessage() + "\n");
            return false;
        }
    }

    public boolean retirarDinero(double monto) {
        if (cuentaActual.getSaldo() > monto){
            cuentaActual.setSaldo(cuentaActual.getSaldo() - monto);
            return true;
        }
        return false;
    }

    public void cambiarClave(String nuevaClave) {
        cuentaActual.setClave(nuevaClave);
    }

    public double obtenerSaldo() {
        return cuentaActual.getSaldo();
    }

    public void imprimirCuentasClienteActual() {
        clienteActual.imprimirCuentas();
    }

    public void imprimirClientes() {
        santotoBank.imprimirClientes();
    }

    public void imprimirColaDeClientes() {
        santotoBank.imprimirColaClientes();
    }

}
